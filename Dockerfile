FROM node:12.22.3-alpine3.14 AS build
RUN cd /app
ADD . .
RUN npm install
ENTRYPOINT ["npm","start"]
